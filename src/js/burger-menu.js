function toggleMenu() {
    const btn = document.getElementsByClassName("burger-menu")[0],
        navList = document.getElementsByClassName("fork__menu")[0];

    btn.addEventListener("click", () => {
        navList.classList.toggle("fork__menu--active");
        btn.classList.toggle("burger-menu--active");
    });
}

toggleMenu();
